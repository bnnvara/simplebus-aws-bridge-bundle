<?php

namespace Tests\BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection;

use BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Configuration;
use BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\SimpleBusAwsBridgeExtension;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionConfigurationTestCase;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Tests\BNNVARA\SimpleBusAwsBridge\Queue\Consumer\DummyEvent;

class ConfigurationTest extends AbstractExtensionConfigurationTestCase
{
    /**
     * @test
     * @dataProvider configurationProvider
     */
    public function configurationsAreValid(array $expected, array $configurationFile)
    {
        $this->assertProcessedConfigurationEquals($expected, $configurationFile);
    }

    public function configurationProvider()
    {
        return [
            [
                [
                    'credentials' => [
                        'key' => 'access_key',
                        'secret' => 'secret_access_key',
                        'token' => null,
                        'expires' => null,
                        'region' => 'eu-west'
                    ]
                ],
                [__DIR__ . '/ConfigurationFixtures/credentials_only_config.yml']
            ],
            [
                [
                    'credentials' => [
                        'key' => 'access_key',
                        'secret' => 'secret_access_key',
                        'token' => 'token',
                        'expires' => 'expires',
                        'region' => 'eu-west'
                    ],
                    'topic' => [
                        'name' => 'topic-arn',
                        'credentials' =>
                            [
                                'key' => 'access_key',
                                'secret' => 'secret_access_key',
                                'token' => 'token',
                                'expires' => 'expires',
                                'region' => 'eu-west'
                            ],
                        'version' => '1.0',
                        'asynchronous_events' => [
                            \stdClass::class,
                            DummyEvent::class
                        ]
                    ],
                    'queue' => [
                        'url' => 'url',
                        'version' => '1.0',
                        'credentials' => [
                            'key' => 'access_key',
                            'secret' => 'secret_access_key',
                            'token' => 'token',
                            'expires' => 'expires',
                            'region' => 'eu-west'
                        ],
                        'envelope_converter' => 'BNNVARA\SimpleBusAwsBridge\Queue\Consumer\SqsToEnvelopeConverter'
                    ]
                ],
                [__DIR__ . '/ConfigurationFixtures/config.yml']
            ],
            [
                [
                    'credentials' => [
                        'key' => 'access_key',
                        'secret' => 'secret_access_key',
                        'token' => null,
                        'expires' => null,
                        'region' => 'eu-west'
                    ],
                    'topic' => [
                        'name' => 'topic-arn',
                        'version' => '1.0',
                        'asynchronous_events' => []
                    ]
                ],
                [__DIR__ . '/ConfigurationFixtures/config_required.yml']
            ],
            [
                [
                    'credentials' => [
                        'key' => 'access_key',
                        'secret' => 'secret_access_key',
                        'token' => 'token',
                        'expires' => 'expires',
                        'region' => 'eu-west'
                    ],
                    'topic' => [
                        'name' => 'topic-arn',
                        'version' => '1.0',
                        'asynchronous_events' => [
                            \stdClass::class,
                            DummyEvent::class
                        ]
                    ]
                ],
                [__DIR__ . '/ConfigurationFixtures/config_global_credentials.yml']
            ],
            [
                [
                    'credentials' => [
                        'key' => 'access_key',
                        'secret' => 'secret_access_key',
                        'token' => null,
                        'expires' => null,
                        'region' => 'eu-west'
                    ],
                    'queue' => [
                        'url' => 'url',
                        'version' => '1.0',
                        'envelope_converter' => 'service-definition-for-converter'
                    ]
                ],
                [__DIR__ . '/ConfigurationFixtures/minimal_queue_configuration.yml']
            ],
            [
                [
                    'credentials' => [
                        'key' => 'access_key',
                        'secret' => 'secret_access_key',
                        'token' => null,
                        'expires' => null,
                        'region' => 'eu-west'
                    ],
                    'queue' => [
                        'url' => 'url',
                        'credentials' => [
                            'key' => 'queue_access_key',
                            'secret' => 'queue_secret_access_key',
                            'token' => null,
                            'expires' => null,
                            'region' => 'queue_eu-west'
                        ],
                        'version' => '1.0',
                        'envelope_converter' => 'service-definition-for-converter'
                    ]
                ],
                [__DIR__ . '/ConfigurationFixtures/maximum_queue_configuration.yml']
            ]
        ];
    }

    /** @inheritdoc */
    protected function getContainerExtension(): ExtensionInterface
    {
        return new SimpleBusAwsBridgeExtension();
    }

    /** @inheritdoc */
    protected function getConfiguration(): ConfigurationInterface
    {
        return new Configuration();
    }
}