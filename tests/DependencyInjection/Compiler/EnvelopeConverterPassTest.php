<?php

namespace Tests\BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Compiler;

use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\SqsConsumer;
use BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Compiler\EnvelopeConverterPass;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

class EnvelopeConverterPassTest extends TestCase
{
    /** @test */
    public function aliasIsSetToConfiguredEnvelopConverter()
    {
        $compilerPass = $this->createEnvelopeConverterPass();

        $compilerPass->process($this->getContainerBuilderWithParameterCall());
    }

    /** @test */
    public function passIsExitedIfQueueIsNotEnabled()
    {
        $compilerPass = $this->createEnvelopeConverterPass();

        $compilerPass->process($this->getContainerWithQueueDisabled());
    }

    /** @test */
    public function exceptionIsThrownIfGivenServiceDoesNotExist()
    {
        $this->expectException(ServiceNotFoundException::class);

        $compilerPass = $this->createEnvelopeConverterPass();

        $compilerPass->process($this->getContainerBuilderWithParameterAndHasCall(false));
    }

    private function createEnvelopeConverterPass(): EnvelopeConverterPass
    {
        return new EnvelopeConverterPass();
    }

    private function getContainerBuilderWithParameterAndHasCall(bool $has): ContainerBuilder
    {
        /** @var MockObject $containerBuilder */
        $containerBuilder = $this->getContainerBuilderMock();

        $containerBuilder->expects($this->once())
            ->method('hasParameter')
            ->with('bnnvara.queue.envelope_converter')
            ->will($this->returnValue(true));

        $containerBuilder->expects($this->once())
            ->method('getParameter')
            ->with('bnnvara.queue.envelope_converter')
            ->will($this->returnValue('service-definition'));

        $containerBuilder->expects($this->once())
            ->method('hasDefinition')
            ->will($this->returnValue($has));

        /** @var ContainerBuilder $containerBuilder */
        return $containerBuilder;
    }

    private function getContainerBuilderWithParameterCall(): ContainerBuilder
    {
        /** @var MockObject $containerBuilder */
        $containerBuilder = $this->getContainerBuilderWithParameterAndHasCall(true);

        $containerBuilder->expects($this->once())
            ->method('getDefinition')
            ->with(SqsConsumer::class)
            ->will($this->returnValue($this->getDefinitionWithSetArgumentCall()));

        /** @var ContainerBuilder $containerBuilder */
        return $containerBuilder;
    }

    private function getContainerBuilderMock(): ContainerBuilder
    {
        $containerBuilderMock = $this->getMockBuilder(ContainerBuilder::class)->disableOriginalConstructor()->getMock();

        /** @var ContainerBuilder $containerBuilderMock */
        return $containerBuilderMock;
    }

    private function getDefinitionWithSetArgumentCall(): Definition
    {
        $definition = $this->getMockBuilder(Definition::class)->disableOriginalConstructor()->getMock();
        $definition->expects($this->once())
            ->method('setArgument')
            ->with(2, new Reference('service-definition'));

        /** @var Definition $definition */
        return $definition;
    }

    private function getContainerWithQueueDisabled()
    {
        /** @var MockObject $containerBuilder */
        $containerBuilder = $this->getContainerBuilderMock();

        $containerBuilder->expects($this->once())
            ->method('hasParameter')
            ->will($this->returnValue(false));

        $containerBuilder->expects($this->never())
            ->method('getParameter');

        /** @var ContainerBuilder $containerBuilder */
        return $containerBuilder;
    }
}