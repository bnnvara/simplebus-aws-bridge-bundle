<?php

namespace Tests\BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Compiler;

use BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Compiler\CollectAsynchronousEventNamesPass;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class CollectAsynchronousEventNamesPassTest extends TestCase
{
    /** @test */
    public function compilerPassIsNotExecutedIfPredefinedMessagesStrategyIsDisabled()
    {
        $compilerPass = $this->getCompilerPass();

        $compilerPass->process($this->getContainerBuilderForNonPredefinedStrategy());
    }

    /** @test */
    public function compilerPassAddsPredefinedMessages()
    {
        $compilerPass = $this->getCompilerPass();

        $compilerPass->process($this->getParameterReplacingContainerBuilder());
    }

    private function getCompilerPass(): CollectAsynchronousEventNamesPass
    {
        return new CollectAsynchronousEventNamesPass();
    }

    private function getParameterReplacingContainerBuilder(): ContainerBuilder
    {
        /** @var MockObject $containerBuilder */
        $containerBuilder = $this->getContainerBuilder();

        $containerBuilder->expects($this->once())
            ->method('hasDefinition')
            ->will($this->returnValue(true));

        $containerBuilder->expects($this->once())
            ->method('getParameter')
            ->with('bnnvara.topic.asynchronous_events')
            ->will($this->returnValue(['one-entry']));

        $containerBuilder->expects($this->once())
            ->method('getDefinition')
            ->will($this->returnValue($this->getDefinitionWithArgumentsCall()));

        /** @var ContainerBuilder $containerBuilder */
        return $containerBuilder;
    }

    private function getContainerBuilderForNonPredefinedStrategy(): ContainerBuilder
    {
        /** @var MockObject $containerBuilder */
        $containerBuilder = $this->getContainerBuilder();

        $containerBuilder->expects($this->once())
            ->method('hasDefinition')
            ->will($this->returnValue(false));

        $containerBuilder->expects($this->never())
            ->method('getDefinition');

        $containerBuilder->expects($this->never())
            ->method('getParameter');

        /** @var ContainerBuilder $containerBuilder */
        return $containerBuilder;
    }

    private function getContainerBuilder(): ContainerBuilder
    {
        $containerBuilder = $this->getMockBuilder(ContainerBuilder::class)->disableOriginalConstructor()->getMock();

        /** @var ContainerBuilder $containerBuilder */
        return $containerBuilder;
    }

    private function getDefinitionWithArgumentsCall(): Definition
    {
        $definition = $this->getMockBuilder(Definition::class)->getMock();
        $definition->expects($this->once())
            ->method('getArgument')
            ->with(2)
            ->will($this->returnValue(['an-other-entry']));

        $definition->expects($this->once())
            ->method('replaceArgument')
            ->with(2, ['one-entry', 'an-other-entry']);

        /** @var Definition $definition */
        return $definition;
    }
}