<?php

namespace Tests\BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection;

use Aws\Sns\SnsClient;
use BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\SimpleBusAwsBridgeExtension;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Symfony\Component\DependencyInjection\Definition;

class SimpleBusAwsBridgeExtensionTest extends AbstractExtensionTestCase
{
    /** @test */
    public function configurationParametersAreMergedToSingleContainerParameter()
    {
        $this->container->setDefinition('simple_bus.asynchronous.message_serializer', new Definition(\stdClass::class));

        $this->load(
            [
                'credentials' => [
                    'key' => 'access_key',
                    'secret' => 'secret_access_key',
                    'region' => 'eu-west'
                ],
                'topic' => [
                    'name' => 'arn:aws:sns:region:account-id:topic-name',
                    'credentials' => [
                        'key' => 'access_key',
                        'secret' => 'secret_access_key',
                        'region' => 'eu-west'
                    ],
                    'version' => '1.0',
                    'asynchronous_events' => []
                ]
            ]
        );

        $this->assertContainerBuilderHasParameter('bnnvara.topic.credentials');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.version');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.region');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.name');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.asynchronous_events');
        $this->assertContainerBuilderHasService('bnnvara.aws_sns_client', SnsClient::class);
    }

    /** @test */
    public function topicServersAreNotLoadedIfTopicIsNotSet()
    {
        $this->container->setDefinition('simple_bus.asynchronous.message_serializer', new Definition(\stdClass::class));

        $this->load(
            [
                'credentials' => [
                    'key' => 'access_key',
                    'secret' => 'secret_access_key',
                    'region' => 'eu-west'
                ]
            ]
        );

        $this->assertContainerBuilderNotHasService('bnnvara.aws_sns_client');
    }

    /** @test */
    public function ifTopicCredentialsAreNotSetGlobalCredentialsAreUsed()
    {
        $this->container->setDefinition('simple_bus.asynchronous.message_serializer', new Definition(\stdClass::class));

        $this->load(
            [
                'credentials' => [
                    'key' => 'access_key',
                    'secret' => 'secret_access_key',
                    'region' => 'eu-west'
                ],
                'topic' => [
                    'name' => 'arn:aws:sns:region:account-id:topic-name',
                    'version' => '1.0',
                    'asynchronous_events' => []
                ]
            ]
        );

        $this->assertContainerBuilderHasParameter(
            'bnnvara.topic.credentials',
            [
                'key' => 'access_key',
                'secret' => 'secret_access_key',
                'token' => null,
                'expires' => null
            ]
        );
        $this->assertContainerBuilderHasParameter('bnnvara.topic.version', '1.0');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.region', 'eu-west');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.name', 'arn:aws:sns:region:account-id:topic-name');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.asynchronous_events', []);
    }

    /** @test */
    public function topicCredentialsGetPreferenceOverGlobalConfiguration()
    {
        $this->container->setDefinition('simple_bus.asynchronous.message_serializer', new Definition(\stdClass::class));

        $this->load(
            [
                'credentials' => [
                    'key' => 'global_access_key',
                    'secret' => 'global_secret_access_key',
                    'region' => 'global_eu-west'
                ],
                'topic' => [
                    'name' => 'arn:aws:sns:region:account-id:topic-name',
                    'credentials' => [
                        'key' => 'topic_access_key',
                        'secret' => 'topic_secret_access_key',
                        'region' => 'topic_eu-west'
                    ],
                    'version' => '1.0',
                    'asynchronous_events' => []
                ],
            ]
        );

        $this->assertContainerBuilderHasParameter(
            'bnnvara.topic.credentials',
            [
                'key' => 'topic_access_key',
                'secret' => 'topic_secret_access_key',
                'token' => null,
                'expires' => null
            ]
        );
        $this->assertContainerBuilderHasParameter('bnnvara.topic.version', '1.0');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.region', 'topic_eu-west');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.name', 'arn:aws:sns:region:account-id:topic-name');
        $this->assertContainerBuilderHasParameter('bnnvara.topic.asynchronous_events', []);
    }

    /** @test */
    public function ifQueueCredentialsAreNotSetDefaultConfigurationIsUsed()
    {
        $this->load(
            [
                'credentials' => [
                    'key' => 'global_access_key',
                    'secret' => 'global_secret_access_key',
                    'region' => 'global_eu-west'
                ],
                'queue' => [
                    'url' => 'url',
                    'version' => '1.0'
                ]
            ]
        );

        $this->assertContainerBuilderHasParameter(
            'bnnvara.queue.credentials',
            [
                'key' => 'global_access_key',
                'secret' => 'global_secret_access_key',
                'token' => null,
                'expires' => null
            ]
        );
        $this->assertContainerBuilderHasParameter('bnnvara.queue.region', 'global_eu-west');
        $this->assertContainerBuilderHasParameter('bnnvara.queue.version', '1.0');
        $this->assertContainerBuilderHasParameter('bnnvara.queue.url', 'url');
    }

    /** @inheritdoc */
    protected function getContainerExtensions(): array
    {
        return [
            new SimpleBusAwsBridgeExtension()
        ];
    }
}