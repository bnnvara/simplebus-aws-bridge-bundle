<?php

namespace Tests\BNNVARA\SimpleBusAwsBridgeBundle\Command;

use BNNVARA\SimpleBusAwsBridge\Queue\QueueName;
use BNNVARA\SimpleBusAwsBridge\Queue\QueueProcessor;
use BNNVARA\SimpleBusAwsBridgeBundle\Command\ConsumeCommand;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

class ConsumeCommandTest extends TestCase
{
    /** @test */
    public function commandIsExecutedWithCorrectArguments()
    {
        $command = $this->getConsumeCommand($this->getQueueProcessorWithConsumeCall(new QueueName('blaat')));

        $command->run(new ArrayInput(['queue' => 'blaat']), new NullOutput());
    }

    /** @test */
    public function anExceptionIsThrownIfQueueParameterIsNotSet()
    {
        $this->expectException(RuntimeException::class);

        $command = $this->getConsumeCommand($this->getQueueProcessor());
        $command->run(new ArrayInput([]), new NullOutput());
    }

    private function getConsumeCommand(QueueProcessor $queueProcessor): ConsumeCommand
    {
        return new ConsumeCommand($queueProcessor);
    }

    private function getQueueProcessorWithConsumeCall(QueueName $queueName): QueueProcessor
    {
        /** @var MockObject $processor */
        $processor = $this->getQueueProcessor();

        $processor->expects($this->once())
            ->method('consume')
            ->with($queueName);

        /** @var QueueProcessor $processor */
        return $processor;
    }

    private function getQueueProcessor(): QueueProcessor
    {
        $processor = $this->getMockBuilder(QueueProcessor::class)->disableOriginalConstructor()->getMock();

        /** @var QueueProcessor $processor */
        return $processor;
    }
}