# Changelog
All notable changes to this project will be documented in this file.
Note: Changelogs are for HUMANS, not computers.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0] - 2022-07-27
### Changed
- Upgrade to PHP 8.1

## [1.2] - 2021-03-22
### Added
- Added supported for PHP 8

## [1.0] - 2020-04-15
- This version is incompatible with anything below Symfony 4.3
- Added docker
- Added fix to use configuration tree builder constructor introduced in Symfony 4.3 (https://symfony.com/blog/symfony-4-3-curated-new-features)
- Use PHP 7.4 in Composer
- Upgraded matthiasnoback/symfony-dependency-injection-test to the latest version (4.1.1)
- Upgraded phpunit/phpunit to version 8.0