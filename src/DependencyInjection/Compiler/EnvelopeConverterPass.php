<?php

namespace BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

class EnvelopeConverterPass implements CompilerPassInterface
{
    /** @throws ServiceNotFoundException */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasParameter('bnnvara.queue.envelope_converter') === false) {
            return;
        }
        $envelopeConverterServiceName = $container->getParameter('bnnvara.queue.envelope_converter');

        if ($container->hasDefinition($envelopeConverterServiceName) === false) {
            throw new ServiceNotFoundException(sprintf('No service found with name %s', $envelopeConverterServiceName));
        }

        $consumerDefintion = $container->getDefinition('BNNVARA\SimpleBusAwsBridge\Queue\Consumer\SqsConsumer');
        $consumerDefintion->setArgument(2, new Reference($envelopeConverterServiceName));
    }
}