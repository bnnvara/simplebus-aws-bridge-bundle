<?php

namespace BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CollectAsynchronousEventNamesPass implements CompilerPassInterface
{
    /** @inheritdoc */
    public function process(ContainerBuilder $container)
    {
        $serviceId = 'simple_bus.asynchronous.publishes_predefined_messages_middleware';
        if (!$container->hasDefinition($serviceId)) {
            return;
        }

        $names = $container->getParameter('bnnvara.topic.asynchronous_events');
        $predefinedMessagesMiddlewareDefinition =  $container->getDefinition($serviceId);
        $alreadyDefinedEventNames = $predefinedMessagesMiddlewareDefinition->getArgument(2);
        $predefinedMessagesMiddlewareDefinition
            ->replaceArgument(2, array_unique(array_merge($names, $alreadyDefinedEventNames)));
    }
}