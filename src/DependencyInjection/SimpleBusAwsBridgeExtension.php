<?php

namespace BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class SimpleBusAwsBridgeExtension extends Extension
{
    /** @inheritdoc */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->handleTopic($container, $config, $loader);
        $this->handleQueue($container, $config, $loader);
    }

    /**
     * @param ContainerBuilder $container
     * @param $config
     * @param XmlFileLoader $loader
     */
    private function handleTopic(ContainerBuilder $container, $config, XmlFileLoader $loader): void
    {
        if (array_key_exists('topic', $config) === false) {
            return;
        }
        $this->handleCredentials($container, $config, 'topic');
        $container->setParameter('bnnvara.topic.version', $config['topic']['version']);
        $container->setParameter('bnnvara.topic.name', $config['topic']['name']);
        $container->setParameter('bnnvara.topic.asynchronous_events', $config['topic']['asynchronous_events']);

        $loader->load('topic.xml');
    }

    private function handleQueue(ContainerBuilder $container, $config, XmlFileLoader $loader): void
    {
        if (array_key_exists('queue', $config) === false) {
            return;
        }

        $this->handleCredentials($container, $config, 'queue');
        $container->setParameter('bnnvara.queue.url', $config['queue']['url']);
        $container->setParameter('bnnvara.queue.version', $config['queue']['version']);
        $container->setParameter('bnnvara.queue.envelope_converter', $config['queue']['envelope_converter']);

        $loader->load('queue.xml');
        $loader->load('command.xml');
    }

    private function handleCredentials(ContainerBuilder $container, array $config, string $part)
    {
        if (array_key_exists('credentials', $config[$part]) === false) {
            $config[$part]['credentials'] = $config['credentials'];
        }

        $container->setParameter(sprintf('bnnvara.%s.region', $part), $config[$part]['credentials']['region']);
        unset($config[$part]['credentials']['region']);
        $container->setParameter(sprintf('bnnvara.%s.credentials', $part), $config[$part]['credentials']);
    }
}