<?php

namespace BNNVARA\SimpleBusAwsBridgeBundle;

use BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Compiler\CollectAsynchronousEventNamesPass;
use BNNVARA\SimpleBusAwsBridgeBundle\DependencyInjection\Compiler\EnvelopeConverterPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SimpleBusAwsBridgeBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new CollectAsynchronousEventNamesPass());
        $container->addCompilerPass(new EnvelopeConverterPass());
    }
}