#BNNVARA/simplebus-aws-bridge-bundle

[![Code Coverage](https://scrutinizer-ci.com/b/bnnvara/simplebus-aws-bridge-bundle/badges/coverage.png?b=develop)](https://scrutinizer-ci.com/b/bnnvara/simplebus-aws-bridge-bundle/?branch=develop)
[![Build Status](https://scrutinizer-ci.com/b/bnnvara/simplebus-aws-bridge-bundle/badges/build.png?b=develop)](https://scrutinizer-ci.com/b/bnnvara/simplebus-aws-bridge-bundle/build-status/develop)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/bnnvara/simplebus-aws-bridge-bundle/badges/quality-score.png?b=develop)](https://scrutinizer-ci.com/b/bnnvara/simplebus-aws-bridge-bundle/?branch=develop)

By [BNNVARA](https://werkenbij.bnnvara.nl/)

#README

##Disclaimer

Be aware: asynchronous events cannot be consumed by the same service that publishes it. Doing so the event will be 
published to the asynchronous queue again, creating an infinite loop. It is possible to bypass this by creating two 
seperate events. One that is handled internally and one that is published to the eventbus.

##Installation

Using Composer:

    composer require bnnvara/simple-bus-aws-bridge-bundle
    
Enable the bundle in `bundles.php`

    return [
            ...
            BNNVARA\SimpleBusAwsBridgeBundle\SimpleBusAwsBridgeBundle::class => ['all' => true],
            ...
    ];

##Configuration

Below a complete overview of the configuration for this bundle

    simple_bus_aws_bridge:
        credentials
            key: value
            secret: value
            token: value
            expires: value
            region: value
        topic:
            name: arn-of-topic
            credentials:
                key: value
                secret: value
                token: value
                expires: value
                region: value
            version: "1.0"
            asynchronous_events:
                - event1
                - event2
        queue:
            url: url-to-sqs-queue
            credentials:
                key: value
                secret: value
                token: value
                expires: value
                region: value
            version: "1.0"
            
The credentials at root level are required. These are the fallback credentials in case there are no specific credentials 
for ```topic``` or ```queue```. This means the credentials section for ```topic``` and ```queue``` are optional and only 
needed if the credentials differ from the root level credentials.

## Topic

The minimal topic configuration looks like this:
    
    simple_bus_aws_bridge:
        credentials
            key: can-be-generated-or-found-in-iam-console-aws
            secret: can-be-generated-or-found-in-iam-console-aws
            region: region-in-which-your-credentials-are-valid
        topic:
            name: arn-of-topic
            version: "version-of-aws-sdk"
            asynchronous_events:
                - first-event-you-want-handled-async
                - second-event-you-want-handled-async
                - etc
                
Only events defined in the ```asynchronous_events``` section are handled asynchronous.

To configure simplebus to use this bridge add/modify the configuration to match this config:

    simple_bus_asynchronous:
      events:
        strategy: 'predefined'
        publisher_service_id: BNNVARA\SimpleBusAwsBridge\Publisher\SnsPublisher

More information on how to configure simplebus please have a look at 
[the documentation](http://docs.simplebus.io/en/latest/ "simplebus documentation")

## Queue
This is the minimal consumer configuration:
    
    simple_bus_aws_bridge:
        credentials
            key: can-be-generated-or-found-in-iam-console-aws
            secret: can-be-generated-or-found-in-iam-console-aws
            region: region-in-which-your-credentials-are-valid
        queue:
            url: 'url'
            version: '1.0'
            
The url of a queue usually looks like ```https://sqs.REGION.amazonaws.com/123456789098/QUEUENAME```. The `url` value in 
the configuration should have the same value **without** the QUEUENAME.

###Consume

Messages are consumed using a console command:

    bnnvara:simplebus:consume <<QUEUENAME>>
    
Assuming the aws url of the queue is ```https://sqs.REGION.amazonaws.com/123456789098/QUEUENAME```, the consume command
look like this:

    bin/console bnnvara:simplebus:consume QUEUENAME
    
